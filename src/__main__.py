from argparse import ArgumentParser
from cProfile import run
from pstats import Stats
from src import Canvas, LIGHT_PALETTE, DARK_PALETTE
from time import strftime
import logging as log


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "-l",
        "--light-theme",
        action="store_true",
        required=False,
        help="Use the light theme",
        dest="light",
    )
    parser.add_argument(
        "-i",
        "--import",
        default="",
        type=str,
        help="Path to previously exported save",
        dest="filename",
    )
    parser.add_argument(
        "-P",
        "--profile",
        action="store_true",
        required=False,
        help="Enables performance profiling",
        dest="profile",
    )
    parser.add_argument(
        "-L",
        "--log-level",
        choices={"debug", "info", "warn", "error", "critical"},
        default="error",
        help="Configure log level",
        dest="log_level",
    )
    args = parser.parse_args()

    palette = LIGHT_PALETTE if args.light else DARK_PALETTE

    log.basicConfig(
        format="{relativeCreated:>10.0f} - {levelname:<8} - {module}:{funcName} - {message}",
        style="{",
        level=args.log_level.upper(),
    )

    if args.profile:
        stats_file = f"./perf/stats-{strftime('%Y%m%dT%H%M%S')}.pstats"
        run(
            "Canvas((1280, 720), palette, filename=args.filename).loop()",
            filename=stats_file,
        )
        stats = Stats(stats_file)
        stats.sort_stats("cumtime").print_stats("src")
    else:
        Canvas((1280, 720), palette, filename=args.filename).loop()
