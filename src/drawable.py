class Drawable:
    def __init__(self):
        self.hidden = False

    def __repr__(self):
        return self.__class__.__name__ + "(" + ", ".join(f"{k}: {v!r}" for k, v in self.__dict__.items()) + ")"

    def __str__(self):
        return repr(self)

    @staticmethod
    def deserialize(data, objs):
        raise NotImplementedError(f"deserialize called with {(data, objs)!r}")

    def allclose(self, other):
        raise NotImplementedError(f"allclose called on {self!r} with {other!r}")

    def serialize(self):
        raise NotImplementedError(f"serialize called on {self!r}")

    def draw(self, color=None):
        raise NotImplementedError(f"draw called on {self!r} with {color!r}")

    def dist(self, pos):
        raise NotImplementedError(f"dist called on {self!r} with {pos!r}")
