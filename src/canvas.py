from enum import Enum, unique
from pygame import Color
from pygame import draw
from pygame import gfxdraw
from src import Drawable, Point, Place, Transition, Arc
from src import MODE_TRIANGLE, FONT_NAME, FONT_SIZE, SCALE_FACTOR, CLICK_MARGIN, OUT_DIR
from time import strftime
import contextlib
import json
import logging as log
import numpy as np
import pygame as pg


def _serialize(obj):
    if isinstance(obj, Color):
        return f"#{obj.r:02x}{obj.g:02x}{obj.b:02x}{obj.a:02x}"
    elif isinstance(obj, np.integer):
        return int(obj)
    elif isinstance(obj, np.floating):
        return float(obj)
    elif isinstance(obj, np.ndarray):
        return list(_serialize(x) for x in obj)
    else:
        raise TypeError(f"{obj!r} of type {type(obj)} is not serializable")


@unique
class Mode(Enum):
    PLACE = "P"
    TRANSITION = "T"
    ARC = "A"

    PLUS = "+"
    MINUS = "-"
    FIRE = "F"

    MOVE = "M"
    DELETE = "D"


class Canvas:
    def __init__(self, res, palette, filename=""):
        # initialize pygame (repeated calls have no effect)
        pg.init()

        # block uninteresting events
        pg.event.set_blocked(
            [
                pg.ACTIVEEVENT,
                pg.JOYAXISMOTION,
                pg.JOYBALLMOTION,
                pg.JOYHATMOTION,
                pg.JOYBUTTONUP,
                pg.JOYBUTTONDOWN,
                pg.VIDEOEXPOSE,
                pg.USEREVENT,
            ]
        )

        # drawing related
        self.palette = palette
        self.font = pg.font.SysFont(FONT_NAME, FONT_SIZE)

        # get screen object
        self.res = np.array(res, dtype="int")
        self.screen = pg.display.set_mode(self.res, pg.RESIZABLE)  # type: ignore

        # pos to px conversion parameters
        self.scale = 1  # in px per unit
        self.offset = self.res.astype("float") / 2  # in px

        # list to store drawable objects
        self.places = []
        self.transitions = []
        self.arcs = []

        # starting mode
        self.mode = Mode.PLACE
        self.selected = None
        self.moving = None
        self.dragging = False

        # preview
        self.mouse_point = Point((0, 0))
        self.preview = None

        # load save
        if len(filename) > 0:
            self.load(filename)

    @property
    def px_transform(self):
        """helper property for Canvas.to_px and Canvas.from_px"""
        return (self.scale, -self.scale)

    def to_px(self, pos):
        """from coordinates to pixel indices"""
        return (pos * self.px_transform + self.offset).astype("int")

    def from_px(self, px):
        """from pixel indices to coordinates"""
        return (px - self.offset).astype("float") / self.px_transform

    def save(self, filename):
        """
        saves the current scene to a file as json, everything from palette to objects is
        stored; the objects are stored in a tree: the key is always the stringified id of
        the current object, the value is a dict containing all information of the object,
        even the defining objects, which are stored in a tuple of ids; or, if the object
        was a fixed point, this tuple represents the floatin point coordinates
        """

        data = {
            "viewport": {"scale": self.scale, "offset": self.offset, "res": self.res},
            "palette": self.palette,
            "places": {id(p): p.serialize() for p in self.places},
            "transitions": {id(t): t.serialize() for t in self.transitions},
            "arcs": {id(a): a.serialize() for a in self.arcs},
        }
        with open(filename, mode="w+") as file:
            return json.dump(data, fp=file, separators=(",", ":"), default=_serialize)

    def load(self, filename):
        """
        loads the previously with Canvas.save exported scene; restores resolution, fixed points,
        hidden/deleted objects, etc
        """
        # clean up this canvas
        del self.places
        del self.transitions
        del self.arcs
        self.places = []
        self.transitions = []
        self.arcs = []
        log.info("cleaned up canvas")

        # load json
        with open(filename) as file:
            data = json.load(file)

        # setup viewport
        vp = data["viewport"]
        self.res = np.array(vp["res"], dtype="int")
        self.screen = pg.display.set_mode(self.res, pg.RESIZABLE)  # type: ignore
        self.scale = vp["scale"]
        self.offset = np.array(vp["offset"], dtype="float")
        self.palette = {k: Color(v) for k, v in data["palette"].items()}

        # load objects (order is important)
        objs = {}
        for place_id, place_ser in data["places"].items():
            place = Place.deserialize(place_ser, objs)
            self.places.append(place)
            objs[place_id] = place
        for transition_id, transition_ser in data["transitions"].items():
            transition = Transition.deserialize(transition_ser, objs)
            self.transitions.append(transition)
            objs[transition_id] = transition
        for arc_id, arc_ser in data["arcs"].items():
            arc = Arc.deserialize(arc_ser, objs)
            self.arcs.append(arc)
            objs[arc_id] = arc

        log.debug("loaded all objects")

    def add(self, obj):
        """
        adds obj to this canvas to be drawn the next tick, obj must have inherited from
        Drawable
        """
        # not drawable
        if not isinstance(obj, Drawable):
            raise TypeError(f"obj {obj!r} is not drawable (is of type {type(obj)})")

        # known type
        elif isinstance(obj, Place):
            self.places.append(obj)
        elif isinstance(obj, Transition):
            self.transitions.append(obj)
        elif isinstance(obj, Arc):
            self.arcs.append(obj)

        # drawable but unknown (not one of the above)
        else:
            raise TypeError(f"Unknown type {type(obj)} of obj {obj!r}")

    def remove(self, obj):
        """
        removes obj from this canvas
        """
        if isinstance(obj, Transition):
            self.transitions.remove(obj)

            # remove every arc which depends on this transition
            self.arcs = list(filter(lambda x: x.transition is not obj, self.arcs))

        if isinstance(obj, Place):
            self.places.remove(obj)

            # remove every arc which depends on this place
            self.arcs = list(filter(lambda x: x.place is not obj, self.arcs))

        if isinstance(obj, Arc):
            obj.weight = 0
            self.arcs.remove(obj)

    def closest(self, px, objs=None):
        """
        returns a tuple (obj, dist) where obj is the object which is currently
        the closest to **px** _on the screen_
        """
        objs = objs or (self.places + self.transitions + self.arcs)
        pos = self.from_px(px)

        closest = None
        dist = float("inf")
        for obj in objs:
            if (x := obj.dist(pos)) < dist:
                dist = x
                closest = obj

        # dist is in units, after dividing by scale its in px
        return (closest, dist * self.scale)

    def clicked_on(self, px, objs=None):
        """
        returns the object in objs that was clicked_on on, or None
        if the click wasnt close to any of those objects
        """
        # clicked on any object
        if objs is None:
            # any point in clickable distance?
            obj, dist = self.closest(px, self.places + self.transitions)
            if dist < CLICK_MARGIN:
                # click it
                return obj

            # just need to check for non point objects now
            objs = self.arcs

        # find closest and check if its close enough
        obj, dist = self.closest(px, objs)
        return obj if dist < CLICK_MARGIN else None

    def select(self, obj):
        """
        selects obj as if it was clicked; returns a tuple of the two objects selected
        objects if there are two, else returns None
        """
        # nothing selected at the moment => select obj
        if self.selected is None:
            self.selected = obj
            return None
        # same object selected => remove object from selection
        elif self.selected is obj:
            self.selected = None
            return None
        # one object already selected => return both selected objects and reset selection
        else:
            ret = (self.selected, obj)
            self.selected = None
            return ret

    def zoom(self, px, dir):
        """
        zooms the canvas towards the position px on screen; dir > 0 zooms in, dir < 0
        zooms out
        """
        factor = 1
        if dir > 0:
            factor = SCALE_FACTOR
        elif dir < 0:
            factor = 1 / SCALE_FACTOR

        self.scale *= factor
        self.offset = (self.offset - px) * factor + px

    def draw(self):
        """
        draws all objects added to this canvas, the mode triangle, etc; everything that needs
        to be draw is drawn hereTOKEN_RADIUS, "foreground", filled=True
        """
        self.screen.fill(self.palette["background"])

        # just skip current frame on error
        try:
            # draw preview if the preview object exists and there is a selected object
            if self.preview and self.selected:
                self.preview.draw(self, "preview")

            # draw all drawables
            for obj_list in (self.arcs, self.transitions, self.places):
                for i, obj in enumerate(obj_list):
                    # only draw close objects once
                    if not any(obj.allclose(x) for x in obj_list[:i]):
                        obj.draw(
                            self,
                            "active" if obj.allclose(self.selected) else "foreground",
                        )
        except (OverflowError, pg.error):
            pass

        # draw triangle in top left corner to show current tool
        gfxdraw.filled_trigon(
            self.screen,
            0,
            0,
            0,
            MODE_TRIANGLE,
            MODE_TRIANGLE,
            0,
            self.palette["inactive"],
        )

        # draw mode letter
        self.draw_text(
            self.from_px((MODE_TRIANGLE / 3, MODE_TRIANGLE / 3)),
            self.mode.value,
            self.palette["foreground"],
        )

        pg.display.flip()

    def draw_text(self, pos, text, color):
        """
        writes text to the screen, x,y-centered at pos
        """
        px = self.to_px(pos)
        color = self.palette[color] if type(color) == str else color

        text = self.font.render(text, True, self.palette["foreground"])  # antialias

        # center on pos
        rect = text.get_rect()  # ez get rekt :D
        rect.center = px

        self.screen.blit(text, rect)

    def draw_circle(self, pos, r, color, filled=True):
        """
        draws a line around the coordinates pos with radius r (in units, not px)
        """
        px = self.to_px(pos)
        r *= self.scale
        color = self.palette[color] if type(color) == str else color

        # for normal size cirlces, use antialiased gfx version
        if min(self.res) > r:
            args = (self.screen, *px, int(r), color)

            gfxdraw.aacircle(*args)  # type: ignore
            if filled:
                gfxdraw.filled_circle(*args)  # type: ignore

        # if it gets too big, gfxdraw acts out (see stackoverflow.com/q/5001070), then use the normal circle
        else:
            draw.circle(self.screen, color, px, int(r), (0 if filled else 1))  # width

    def draw_polygon(self, points, color, filled=True):
        """
        draw a polygon
        """
        pxs = [self.to_px(p) for p in points]
        color = self.palette[color] if type(color) == str else color

        args = (self.screen, pxs, color)

        gfxdraw.aapolygon(*args)
        if filled:
            gfxdraw.filled_polygon(*args)

    def draw_rect(self, topleft, bottomright, color, filled=True):
        """
        draws a rectangle
        """
        tl = self.to_px(topleft)
        br = self.to_px(bottomright)
        color = self.palette[color] if type(color) == str else color
        if filled:
            gfxdraw.box(self.screen, (tl, br - tl), color)
        else:
            gfxdraw.rectangle(self.screen, (tl, br - tl), color)

    def set_mode(self, mode_letter):
        """
        sets the current mode to the mode associated by mode_letter, if it exists, else just
        resets the preview and the selection
        """
        # update mode
        with contextlib.suppress(ValueError):
            self.mode = Mode(mode_letter.upper())

        # remove selection
        self.selected = None
        self.preview = None

    def mouse_click(self, px):
        """
        handles a mouse click on the pixel px according to the current mode
        """
        if self.mode == Mode.PLACE:
            self.add(Place(self.from_px(px)))

        elif self.mode == Mode.TRANSITION:
            self.add(Transition(self.from_px(px)))

        elif self.mode == Mode.ARC:
            clickable = ([] if isinstance(self.selected, Place) else self.places) + (
                [] if isinstance(self.selected, Transition) else self.transitions
            )

            if clicked := self.clicked_on(px, clickable):
                if objs := self.select(clicked):
                    # add io to transition
                    if isinstance(objs[0], Transition):
                        objs[0].add_output(objs[1])
                    elif isinstance(objs[1], Transition):
                        objs[1].add_input(objs[0])

                    # add real object
                    for arc in self.arcs:
                        if arc.orig == objs[0] and arc.dest == objs[1]:
                            arc.weight += 1
                    else:
                        self.add(Arc(*objs))

                # nothing currently selected => remove preview
                if self.selected is None:
                    self.preview = None
                # one object was selected, draw preview from it to the cursor
                else:
                    self.preview = Arc(self.selected, self.mouse_point)

        elif self.mode == Mode.PLUS:
            if clicked := self.clicked_on(px):
                if isinstance(clicked, Place):
                    clicked.tokens += 1
                elif isinstance(clicked, Arc):
                    clicked.weight += 1

        elif self.mode == Mode.MINUS:
            if clicked := self.clicked_on(px, self.places + self.arcs):
                if isinstance(clicked, Place):
                    clicked.tokens -= 1
                elif isinstance(clicked, Arc):
                    if clicked.weight == 1:
                        self.remove(clicked)
                    else:
                        clicked.weight -= 1

        elif self.mode == Mode.FIRE:
            if clicked := self.clicked_on(px, self.transitions):
                clicked.fire()

        elif self.mode == Mode.DELETE:
            if obj := self.clicked_on(px):
                self.remove(obj)

        elif self.mode == Mode.MOVE:
            if obj := self.clicked_on(px, self.places + self.transitions):
                self.moving = obj

        else:
            raise ValueError("Unknown mode")

    def tick(self):
        """
        ticks the game forwards, check events, mouse clicks, etc
        """
        done = False

        # check event queue
        for event in pg.event.get():
            # quit
            if event.type == pg.QUIT:
                done = True

            # window resized
            elif event.type == pg.VIDEORESIZE:
                self.res = np.array(event.size, dtype="int")
                self.screen = pg.display.set_mode(event.size, pg.RESIZABLE)

            # key pressed
            elif event.type == pg.KEYDOWN:
                self.set_mode(event.unicode)

                # check for export
                if event.unicode == "x":
                    filename = OUT_DIR + "save-" + strftime("%Y%m%dT%H%M%S") + ".json"
                    self.save(filename)
                    print(f"saved to {filename}")

                # check for q or ctrl c
                if event.unicode == "q" or (event.key == pg.K_c and pg.key.get_mods() & pg.KMOD_CTRL):
                    done = True

            # mouse scroll zooms
            elif event.type == pg.MOUSEBUTTONDOWN and event.button in {
                pg.BUTTON_WHEELUP,
                pg.BUTTON_WHEELDOWN,
            }:
                self.zoom(
                    np.array(event.pos, dtype="float"),
                    1 if event.button == pg.BUTTON_WHEELUP else -1,
                )

            # mouse click
            elif event.type == pg.MOUSEBUTTONDOWN:
                # left click action depends on mode
                if event.button == pg.BUTTON_LEFT:
                    self.mouse_click(np.array(event.pos, dtype="float"))

                # holding right click drags the canvas
                if event.button == pg.BUTTON_RIGHT:
                    self.dragging = True

            # mouse button released
            elif event.type == pg.MOUSEBUTTONUP:
                # left mouse button release => stop moving point
                if event.button == pg.BUTTON_LEFT:
                    self.moving = None

                # right mouse button released => stop dragging
                if event.button == pg.BUTTON_RIGHT:
                    self.dragging = False

            # mouse moved
            elif event.type == pg.MOUSEMOTION:
                # update mouse point
                self.mouse_point.pos = self.from_px(event.pos)

                # move canvas, if dragging
                if self.dragging:
                    self.offset += event.rel

                # move point, if one selected
                if self.moving:
                    self.moving.pos = self.from_px(self.to_px(self.moving.pos) + event.rel)

        # render
        self.draw()

        return done

    def loop(self):
        """
        the main loop; ticks forward until closed
        """
        while not self.tick():
            pass
