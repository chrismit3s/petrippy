from src import Drawable
import numpy as np


class Point(Drawable):
    def __init__(self, pos):
        self.pos = np.array(pos, dtype="float64")

    def __repr__(self):
        return f"{self.__class__.__name__}({self.pos[0]:.2f}, {self.pos[1]:.2f})"

    def __str__(self):
        return repr(self)

    def allclose(self, other):
        return isinstance(other, Point) and np.allclose(self.pos, other.pos)

    def dist(self, pos):
        return np.linalg.norm(self.pos - pos)
