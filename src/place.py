from src import _rotation_matrix
from src import Point
from src import POINT_RADIUS, TOKEN_RADIUS, BORDER_WIDTH
import numpy as np


class Place(Point):
    def __init__(self, pos, capacity=float("inf")):
        super().__init__(pos)
        self.capacity = capacity
        self._tokens = 0

    @staticmethod
    def deserialize(data, _):
        place = Place(data["pos"], capacity=data["capacity"])
        place._tokens = data["tokens"]
        return place

    @property
    def tokens(self):
        return self._tokens

    @tokens.setter
    def tokens(self, value, /):
        if 0 <= value < self.capacity:
            self._tokens = value
        else:
            raise ValueError(
                f"Number of tokens in place must be between 0 and its capacity (={self.capacity}), can't set it to {value}"
            )

    def serialize(self):
        return {
            "pos": self.pos,
            "capacity": self.capacity,
            "tokens": self.tokens,
        }

    def draw(self, canvas, color="foreground"):
        canvas.draw_circle(self.pos, POINT_RADIUS, color, filled=True)
        canvas.draw_circle(self.pos, POINT_RADIUS - BORDER_WIDTH, "background", filled=True)  # leave only a border

        if self.tokens == 1:
            canvas.draw_circle(self.pos, TOKEN_RADIUS, "foreground", filled=True)
        elif self.tokens <= 3:
            for angle in np.linspace(0, 2 * np.pi, self.tokens, endpoint=False):
                canvas.draw_circle(
                    self.pos + _rotation_matrix(angle) @ [0, (POINT_RADIUS - BORDER_WIDTH) / 2],  # type: ignore
                    TOKEN_RADIUS,
                    "foreground",
                    filled=True,
                )
        elif self.tokens > 3:
            canvas.draw_text(self.pos, str(self.tokens), "foreground")
