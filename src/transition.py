from src import POINT_RADIUS, BORDER_WIDTH
from src import Point, Place
from src import _rotation_matrix
from collections import defaultdict
import numpy as np


class Transition(Point):
    _outer_offset = _rotation_matrix(np.arctan(1 / 4)) @ [
        0,
        POINT_RADIUS,
    ]  # get a rect with an aspect ration of 4:1
    _inner_offset = _outer_offset - BORDER_WIDTH

    def __init__(self, pos):
        super().__init__(pos)
        self.inputs = defaultdict(lambda: 0)
        self.outputs = defaultdict(lambda: 0)

    @staticmethod
    def deserialize(data, objs):
        transition = Transition(data["pos"])
        for id, weight in data["inputs"].items():
            place = objs[id]
            transition.inputs[place] = weight
        for id, weight in data["outputs"].items():
            place = objs[id]
            transition.outputs[place] = weight
        return transition

    def add_input(self, place):
        if not isinstance(place, Place):
            raise TypeError(f"Input places must be of type {Place}, not {type(place)}")
        self.inputs[place] += 1

    def add_output(self, place):
        if not isinstance(place, Place):
            raise TypeError(f"Output places must be of type {Place}, not {type(place)}")
        self.outputs[place] += 1

    def can_fire(self):
        for place, weight in self.inputs.items():
            if place.tokens < weight:
                return False

        for place, weight in self.outputs.items():
            # if the remaining capacity of the place after firing is enough to hold the new tokens
            if (place.capacity - (place.tokens - self.inputs[place])) < weight:
                return False

        return sum(self.inputs.values()) + sum(self.outputs.values()) > 0

    def fire(self):
        if not self.can_fire():
            return False

        for place, weight in self.inputs.items():
            place.tokens -= weight
        for place, weight in self.outputs.items():
            place.tokens += weight
        return True

    def serialize(self):
        return {
            "pos": self.pos,
            "inputs": {
                str(
                    id(place)
                ): weight  # str(id(...)) as json strings dict keys which can make for nasty key not found errors
                for place, weight in self.inputs.items()
                if weight != 0
            },
            "outputs": {
                str(
                    id(place)
                ): weight  # str(id(...)) as json strings dict keys which can make for nasty key not found errors
                for place, weight in self.outputs.items()
                if weight != 0
            },
        }

    def draw(self, canvas, color="foreground"):
        canvas.draw_rect(
            self.pos - Transition._outer_offset,
            self.pos + Transition._outer_offset,
            color,
            filled=True,
        )
        canvas.draw_rect(
            self.pos - Transition._inner_offset,
            self.pos + Transition._inner_offset,
            "active" if self.can_fire() else color,
            filled=True,
        )
