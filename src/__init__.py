from pygame import Color
from os.path import join


POINT_RADIUS = 20  # units
TOKEN_RADIUS = 5  # units
BORDER_WIDTH = 3  # units

MODE_TRIANGLE = 60  # px

FONT_NAME = "Roboto Mono Medium for Powerline"
FONT_SIZE = 20  # pt

SCALE_FACTOR = 0.9  # px/unit
CLICK_MARGIN = 20  # px

OUT_DIR = join(".", "out", "")

# see coolors.co/f9f1ed-ccc4c5-201d1b-60c198-3d6e90
LIGHT_PALETTE = {
    "background": Color("#F9F1ED"),
    "foreground": Color("#201D1B"),
    "active": Color("#60C198"),
    "inactive": Color("#CCC4C5"),
    "preview": Color("#3D6E90"),
}
# see coolors.co/0f0f10-3e3d3b-e0dfd5-f06543-b9d389
DARK_PALETTE = {
    "background": Color("#0F0F10"),
    "foreground": Color("#E0DFD5"),
    "active": Color("#F06543"),
    "inactive": Color("#3E3D3B"),
    "preview": Color("#B9D389"),
}


from src.helpers import _rotation_matrix

from src.drawable import Drawable
from src.point import Point
from src.place import Place
from src.transition import Transition
from src.arc import Arc

from src.canvas import Canvas

__all__ = ["_rotation_matrix", "Drawable", "Point", "Place", "Transition", "Arc", "Canvas"]
