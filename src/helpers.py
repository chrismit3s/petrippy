import numpy as np


def _rotation_matrix(rad):
    return np.array([[np.cos(rad), np.sin(rad)], [-np.sin(rad), np.cos(rad)]])
