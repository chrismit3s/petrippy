from src import _rotation_matrix
from src import Drawable, Transition, Place, Point
from src import BORDER_WIDTH, POINT_RADIUS
import numpy as np


class Arc(Drawable):
    def __init__(self, orig, dest, weight=1):
        super().__init__()

        if {type(orig), type(dest)} != {Transition, Place} and type(
            dest
        ) != Point:  # if dest is a Point its the mouse point for the preview
            raise TypeError(
                f"Arcs must always connect a place with a transition or vice-versa (not a {type(orig)} with a {type(dest)})"
            )

        self.transition = self.place = self.is_input = None
        if isinstance(orig, Transition):
            self.transition = orig
            self.place = dest
            self.is_input = False
        elif isinstance(orig, Place):
            self.place = orig
            self.transition = dest
            self.is_input = True

        self._weight = weight

    @staticmethod
    def deserialize(data, objs):
        orig = objs[data["orig"]]
        dest = objs[data["dest"]]
        weight = data["weight"]
        return Arc(orig, dest, weight)

    @property
    def orig(self):
        return self.place if self.is_input else self.transition

    @property
    def dest(self):
        return self.transition if self.is_input else self.place

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, value, /):
        self._weight = value
        if self.is_input:
            self.transition.inputs[self.place] = value  # type: ignore
        else:
            self.transition.outputs[self.place] = value  # type: ignore

    def allclose(self, other):
        return (
            isinstance(other, Arc)
            and np.allclose(self.orig.pos, other.orig.pos)  # type: ignore
            and np.allclose(self.dest.pos, other.dest.pos)  # type: ignore
        )

    def serialize(self):
        return {
            "orig": str(
                id(self.orig)
            ),  # str(id(...)) as json strings dict keys which can make for nasty key not found errors
            "dest": str(id(self.dest)),
            "is_input": self.is_input,
            "weight": self.weight,
        }

    def draw(self, canvas, color="foreground"):
        #                  C1'\
        # A1---------------B1  '\
        # A0               B0 C0 :C1
        # A1---------------B1  ./
        #                  C1./
        diff = self.dest.pos - self.orig.pos  # type: ignore
        dist = np.linalg.norm(diff)
        dir = diff / dist
        perp = _rotation_matrix(np.pi / 2) @ dir

        orig = self.orig.pos + (POINT_RADIUS * dir if self.is_input else 0)  # type: ignore
        dist -= POINT_RADIUS

        # draw tail (get base points A0 and B0, plusminus offset for all for corners)
        base_a = orig + dir * BORDER_WIDTH / 2
        base_b = orig + dir * (
            dist - POINT_RADIUS * 3 / 4
        )  # 3/4 for a equilateral triangle which has a cirumcircle with a diameter of POINT_RADIUS
        offset = perp * BORDER_WIDTH / 2
        tail_points = [
            base_a - offset,
            base_a + offset,
            base_b + offset,
            base_b - offset,
        ]
        canvas.draw_polygon(tail_points, color)
        canvas.draw_circle(base_a, BORDER_WIDTH / 2, color)

        # draw head (get center point C0)
        center = orig + dir * (dist - POINT_RADIUS / 2)
        offset = dir * POINT_RADIUS / 2
        canvas.draw_polygon(
            [center + _rotation_matrix(angle) @ offset for angle in np.linspace(0, 2 * np.pi, 3, endpoint=False)],
            color,
        )

        if self.weight > 1:
            canvas.draw_text(
                orig + dir * dist * 2 / 3 + perp * POINT_RADIUS / 2,
                str(self.weight),
                "foreground",
            )

    def dist(self, pos):
        diff = self.dest.pos - self.orig.pos  # type: ignore
        dist = np.linalg.norm(diff)
        dir = diff / dist

        to_orig = pos - self.orig.pos  # type: ignore
        to_dest = pos - self.dest.pos  # type: ignore
        dist_orig = np.linalg.norm(to_orig)
        dist_dest = np.linalg.norm(to_dest)

        alpha = np.dot(to_orig, dir) / dist_orig  # dir is already length 1
        beta = np.dot(to_dest, -dir) / dist_dest  # dir is already length 1

        # pos is on the orig side
        if alpha < 0:
            return dist_orig  # distance to the origin point
        # pos is on the dest side
        elif beta < 0:
            return dist_dest  # distance to the destination point
        # pos is between orig and dest
        else:
            perp = _rotation_matrix(np.pi / 2) @ dir
            return abs(np.dot(to_orig, perp))  # perpendicular projection of x on a vector perpendicular to this arc
