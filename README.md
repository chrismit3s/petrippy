# petrippy

A [Petri net](https://en.wikipedia.org/wiki/Petri_net) simulator.

## Setup

Install the packages from `requirements.txt` with `pip install -r requirements.txt`,
then run the project with `python -m src`.

## Keybindings

Right click moves the canvas, mouse wheel zooms, the left click action depends on the mode:

| Name         | Key     | Function |
| ------------ | ------- | -------- |
| Node         | `P`     | Place a node which hold tokens |
| Transition   | `T`     | Place a transition which can fire if all input nodes connected to it contain the required amount of tokens |
| Arc          | `A`     | Connect a node and a transition |
| In-/Decrease | `+`/`-` | When clicking on a node: increase the number of tokens it holds, when clicking on an arc: increase the number of tokens it requires/generates |
| Fire         | `F`     | Fire a transition (if its conditions are met) |
| Move         | `M`     | Move a node or transition by clicking and dragging |
| Delete       | `D`     | Delete a node or transition |
| Export       | `X`     | Exports the current scene to a JSON file in the `out` directory, to load a previously exported scene, use the `-i <PATH>` flag |
| Quit         | `Q`     | Quits |

